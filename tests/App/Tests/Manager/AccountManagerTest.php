<?php

namespace App\Tests\Manager;

use App\Exception\AccountDepositMaxLimitException;
use App\Exception\AccountStatusException;
use App\Exception\BadAmountException;
use App\Exception\InsufficientFundsAccountException;
use App\Exception\OverdraftMaxLimitException;
use App\Manager\AccountManager;
use App\Model\Account;
use App\Storage\AccountStorageInterface;
use PHPUnit\Framework\TestCase;
class AccountManagerTest extends TestCase
{
    /** @var \PHPUnit_Framework_MockObject_MockObject|AccountStorageInterface */
    protected $accountStorageMock;

    /** @var AccountManager */
    protected $accountManager;

    public function setUp() : void
    {
        $this->accountStorageMock = $this->createMock(AccountStorageInterface::class);
        $this->accountManager = new AccountManager($this->accountStorageMock, 1000 * 100, 1000* 100);
    }

    public function constructorDataProvider() : array
    {
        return [
            [0, 0, true],
            [10000, 0, true],
            [0, 12345, true],
            [-1, 0, true],
            [0, -1000, true],
            [-1, 10000, true],
            [10000, -1, true],
            [-1, -1, true],
            [-100000, -10000, true],
            [1, 100000, false],
            [100000, 10, false],
            [PHP_INT_MAX, PHP_INT_MAX, false]
        ];
    }

    /**
     * @dataProvider constructorDataProvider
     * @param int $maxDepositAmount
     * @param int $maxOverDraftAmount
     * @param bool $expectedException
     */
    public function testConstructorNegativeIntegersException(int $maxDepositAmount, int $maxOverDraftAmount, bool $expectedException) : void
    {
        if ($expectedException) {
            $this->expectException(BadAmountException::class);
        }

        $this->accountManager = new AccountManager($this->accountStorageMock, $maxDepositAmount, $maxOverDraftAmount);
        $this->assertTrue(true);
    }

    public function testGetCurrent() : void
    {
        $this->accountStorageMock
            ->expects($this->once())
            ->method('getCurrent')
        ;

        $this->accountManager->getCurrent();
    }

    public function closeDataProvider() : array
    {
        return [
            [Account::STATUS_OPEN, false],
            [Account::STATUS_CLOSED, true]
        ];
    }

    /**
     * @dataProvider closeDataProvider
     * @param int $status
     * @param bool $exception
     */
    public function testClose(int $status, bool $exception) : void
    {
        $currentAccount = new Account();
        $currentAccount
            ->setStatus($status);
        ;

        if ($exception) {
            $this->expectException(AccountStatusException::class);
        } else {
            $this->accountStorageMock
                ->expects($this->once())
                ->method('update')
                ->with($this->equalTo($currentAccount))
            ;
        }

        $this->accountManager->close($currentAccount);
        $this->assertEquals($currentAccount->getStatus(), Account::STATUS_CLOSED);
    }

    public function isCurrentOpenDataProvider() : array
    {
        $openAccount = new Account();
        $openAccount->setStatus(Account::STATUS_OPEN);

        $closedAccount = new Account();
        $closedAccount->setStatus(Account::STATUS_CLOSED);

        return [
            [null, false],
            [$closedAccount, false],
            [$openAccount, true]
        ];
    }

    /**
     * @dataProvider isCurrentOpenDataProvider
     * @param Account|null $account
     * @param bool $isOpen
     */
    public function testIsCurrentOpen(?Account $account, bool $isOpen): void
    {
        $accManagerMock = $this->getMockBuilder(AccountManager::class)
            ->setConstructorArgs([$this->accountStorageMock, 1000 * 100, 1000 * 100])
            ->setMethods(['getCurrent'])
            ->getMock()
        ;

        $accManagerMock->expects($this->once())
            ->method('getCurrent')
            ->will($this->returnValue($account))
        ;

        $this->assertEquals($accManagerMock->isCurrentOpen(), $isOpen);
    }

    public function openCurrentDataProvider()
    {
        return [
            [true, true],
            [false, false]
        ];
    }

    /**
     * @dataProvider openCurrentDataProvider
     * @param bool $isCurrentOpen
     * @param bool $exception
     */
    public function testOpenCurrent(bool $isCurrentOpen, bool $exception) : void
    {
        $accManagerMock = $this->getMockBuilder(AccountManager::class)
            ->setConstructorArgs([$this->accountStorageMock, 1000 * 100, 1000*100])
            ->setMethods(['isCurrentOpen'])
            ->getMock()
        ;

        $accManagerMock->expects($this->once())
            ->method('isCurrentOpen')
            ->will($this->returnValue($isCurrentOpen))
        ;

        if ($exception) {
            $this->expectException(AccountStatusException::class);
        } else {
            $this->accountStorageMock
                ->expects($this->once())
                ->method('createNew')
            ;
        }

        $accManagerMock->openCurrent();
    }

    public function depositDataProvider() : array
    {
        return [
            [Account::STATUS_OPEN, -100000, 100000, 0, null],
            [Account::STATUS_OPEN, 0, 100000, 100000, null],
            [Account::STATUS_OPEN, 0, 1, 1, null],
            [Account::STATUS_CLOSED, -1000, 1000, 0, AccountStatusException::class],
            [Account::STATUS_OPEN, -1000, 0, -1000, BadAmountException::class],
            [Account::STATUS_OPEN, -1000, -150, -1150, BadAmountException::class],
            [Account::STATUS_OPEN, -100000, 200100, 100100, AccountDepositMaxLimitException::class],
            [Account::STATUS_OPEN, 0, 1000001, 1000001, AccountDepositMaxLimitException::class],
        ];
    }

    /**
     * @dataProvider depositDataProvider
     * @param int $status
     * @param int $balance
     * @param int $amount
     * @param int $newBalance
     * @param null|string $exceptionClass
     */
    public function testDepositFunds(
        int $status,
        int $balance,
        int $amount,
        int $newBalance,
        ?string $exceptionClass
    ) : void
    {
        $account = new Account();
        $account
            ->setBalance($balance)
            ->setStatus($status)
        ;

        if ($exceptionClass) {
            $this->expectException($exceptionClass);
        } else {
            $this->accountStorageMock
                ->expects($this->once())
                ->method('update')
                ->with($this->equalTo($account))
            ;
        }

        $this->accountManager->depositFunds($account, $amount);
        $this->assertEquals($account->getBalance(), $newBalance);
    }

    public function withdrawDataProvider() : array
    {
        return [
            [Account::STATUS_OPEN, 100, 100, 0, false, null],
            [Account::STATUS_OPEN, 100, 100, 0, true, null],
            [Account::STATUS_OPEN, 201, 100, 101, false, null],
            [Account::STATUS_OPEN, 201, 100, 101, true, null],
            [Account::STATUS_OPEN, 9000, 1000, 8000, false, null],
            [Account::STATUS_OPEN, 9000, 1000, 8000, true, null],
            [Account::STATUS_OPEN, 9000, 10000, -1000, false, InsufficientFundsAccountException::class],
            [Account::STATUS_OPEN, 9000, 10000, -1000, true, null],
            [Account::STATUS_CLOSED, 100, 100, 0, false, AccountStatusException::class],
            [Account::STATUS_CLOSED, 100, 100, 0, true, AccountStatusException::class],
            [Account::STATUS_OPEN, 1000, 0, 1000, false, BadAmountException::class],
            [Account::STATUS_OPEN, 1000, 0, 1000, true, BadAmountException::class],
            [Account::STATUS_OPEN, -1000, -150, -1150, false, BadAmountException::class],
            [Account::STATUS_OPEN, -1000, -150, -1150, true, BadAmountException::class],
            [Account::STATUS_OPEN, 100000, 200100, -100100, true, OverdraftMaxLimitException::class],
            [Account::STATUS_OPEN, 100000, 200100, -100100, false, InsufficientFundsAccountException::class],
        ];
    }

    /**
     * @dataProvider withdrawDataProvider
     * @param int $status
     * @param int $balance
     * @param int $amount
     * @param int $newBalance
     * @param bool $applyOverDraft
     * @param null|string $exception
     */
    public function testWithdrawFunds(
        int $status,
        int $balance,
        int $amount,
        int $newBalance,
        bool $applyOverDraft,
        ?string $exception
    ) : void
    {
        $account = new Account();
        $account
            ->setStatus($status)
            ->setBalance($balance)
        ;

        if ($exception) {
            $this->expectException($exception);
        } else {
            $this->accountStorageMock
                ->expects($this->once())
                ->method('update')
                ->with($this->equalTo($account))
            ;
        }

        $this->accountManager->withdrawFunds($account, $amount, $applyOverDraft);
        $this->assertEquals($account->getBalance(), $newBalance);
    }
}