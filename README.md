# Description #

* This application simulates current bank account through a command line interface.
* Created by Daumantas Urbanavičius

# Requirements #

- PHP 7.1+
- Composer (http://www.getcomposer.org)

# Installation #

* Run following command:
````
php composer install
````
# How to use application #

* Run following command:
````
php bin/console app:current-account
````
* Follow the steps through console

# How to run tests #

* Run following command:
````
php vendor/phpunit/phpunit/phpunit
````

# Overview #

The bank account cli application has following operations:

* Open account
* Apply overdraft
* Deposit funds
* Withdraw funds
* Display balance
* Close account

PHPUnit Unit tests are used to test the business logic of application.

This application is using following features of PHP7:
* function parameter types
* function return types
* nullable types

# Bonus #

* Exception system that deals with account operations
* Minimal file cache storage class
