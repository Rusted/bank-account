<?php

namespace App\Manager;

use App\Exception\AccountDepositMaxLimitException;
use App\Exception\AccountStatusException;
use App\Exception\BadAmountException;
use App\Exception\InsufficientFundsAccountException;
use App\Exception\OverdraftMaxLimitException;
use App\Model\Account;
use App\Storage\AccountStorageInterface;

class AccountManager
{
    /** @var AccountStorageInterface */
    protected $storage;

    /** @var int */
    protected $maxDepositAmount;

    /** @var int */
    protected $maxOverdraftAmount;

    /**
     * AccountManager constructor.
     * @param AccountStorageInterface $storage
     * @param int $maxDepositAmount
     * @param int $maxOverdraftAmount
     * @throws BadAmountException
     */
    public function __construct(AccountStorageInterface $storage, int $maxDepositAmount, int $maxOverdraftAmount)
    {
        if ($maxDepositAmount <= 0) {
            throw new BadAmountException('Bad amount. Max deposit amount must be greater than 0!');
        }

        if ($maxOverdraftAmount <= 0) {
            throw new BadAmountException('Bad amount. Max overdraft amount must be greater than 0!');
        }

        $this->storage = $storage;
        $this->maxDepositAmount = $maxDepositAmount;
        $this->maxOverdraftAmount = $maxOverdraftAmount;
    }

    /**
     * @return Account|null
     */
    public function getCurrent() : ?Account
    {
        return $this->storage->getCurrent();
    }

    public function openCurrent() : bool
    {
        if ($this->isCurrentOpen()) {
            throw new AccountStatusException('Current account is already open!');
        }

        $account = $this
            ->create()
            ->setBalance(0)
            ->setStatus(Account::STATUS_OPEN)
        ;

        return $this->storage->createNew($account);
    }

    /**
     * @return bool
     */
    public function isCurrentOpen() : bool
    {
        $currentAccount = $this->getCurrent();

        return $currentAccount instanceof Account && $currentAccount->isOpen();
    }

    /**
     * @param Account $account
     * @return bool
     * @throws AccountStatusException
     */
    public function close(Account $account) : bool
    {
        if (!$account->isOpen()) {
            throw new AccountStatusException('Account is closed!');
        }

        $account->setStatus(Account::STATUS_CLOSED);

        return $this->storage->update($account);
    }

    /**
     * @param Account $account
     * @param int $amount
     * @return bool
     * @throws AccountDepositMaxLimitException
     * @throws AccountStatusException
     * @throws BadAmountException
     */
    public function depositFunds(Account $account, int $amount) : bool
    {
        if (!$account->isOpen()) {
            throw new AccountStatusException('Account is not opened!');
        }

        if ($amount <= 0) {
            throw new BadAmountException('Bad amount. Deposit amount must be greater than 0!');
        }

        $newBalance = $account->getBalance() + $amount;
        if ($newBalance > $this->maxDepositAmount) {
            throw new AccountDepositMaxLimitException('Failed to deposit. Over maximum deposit limit!');
        }

        $account->setBalance($newBalance);
        return $this->storage->update($account);
    }

    /**
     * @param Account $account
     * @param int $amount
     * @param bool $applyOverdraft
     * @return bool
     * @throws AccountStatusException
     * @throws BadAmountException
     * @throws InsufficientFundsAccountException
     * @throws OverdraftMaxLimitException
     */
    public function withdrawFunds(Account $account, int $amount, bool $applyOverdraft = false) : bool
    {
        if (!$account->isOpen()) {
            throw new AccountStatusException('Account is not opened!');
        }

        if ($amount <= 0) {
            throw new BadAmountException('Bad amount. Withdraw amount must be greater than 0!');
        }

        $newBalance = $account->getBalance() - $amount;
        if ($applyOverdraft) {
            if ($newBalance < -$this->maxOverdraftAmount) {
                throw new OverdraftMaxLimitException('Failed to apply overdraft. Over maximum overdraft limit!');
            }
        } else {
            if ($newBalance < 0) {
                throw new InsufficientFundsAccountException('Failed to withdraw. Insufficient funds!');
            }
        }

        $account->setBalance($newBalance);

        return $this->storage->update($account);
    }

    /**
     * @return Account
     */
    public function create() : Account
    {
        return new Account();
    }
}
