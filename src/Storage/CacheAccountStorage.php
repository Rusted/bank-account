<?php

namespace App\Storage;

use App\Model\Account;
use Psr\Cache\CacheItemPoolInterface;

class CacheAccountStorage implements AccountStorageInterface
{
    const CACHE_KEY_PREFIX = 'ACCOUNT_';
    const CACHE_KEY_CURRENT_ACCOUNT = 'CURRENT_ACCOUNT_';
    const CACHE_KEY_CURRENT_ACCOUNT_ID = 'CURRENT_ACCOUNT_ID';

    /** @var CacheItemPoolInterface */
    protected $cache;

    /**
     * AccountStorage constructor.
     * @param CacheItemPoolInterface $cache
     */
    public function __construct(CacheItemPoolInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return Account|null
     */
    public function getCurrent() : ?Account
    {
        $id = $this->getCurrentId();
        if (is_null($id)) {
            return null;
        }

        return $this->getAccount($id);
    }

    /**
     * @param int $id
     * @return Account|null
     */
    protected function getAccount(int $id) : ?Account
    {
        $item = $this->cache->getItem(self::CACHE_KEY_PREFIX . self::CACHE_KEY_CURRENT_ACCOUNT . $id);

        return $item->get();
    }

    /**
     * @return int|null
     */
    public function getCurrentId() : ?int
    {
        $item = $this->cache->getItem(self::CACHE_KEY_PREFIX . self::CACHE_KEY_CURRENT_ACCOUNT_ID);

        return $item->get();
    }

    /**
     * @param $id
     * @return bool
     */
    private function setCurrentId($id) : bool
    {
        $item = $this->cache->getItem(self::CACHE_KEY_PREFIX . self::CACHE_KEY_CURRENT_ACCOUNT_ID);
        $item->set($id);
        return $this->cache->save($item);
    }

    /**
     * @param Account $account
     * @return bool
     */
    public function createNew(Account $account) : bool
    {
        $id = $this->getCurrentId();
        $newId = (int)$id + 1;
        $account
            ->setId($newId)
        ;

        $item = $this->cache->getItem(self::CACHE_KEY_PREFIX . self::CACHE_KEY_CURRENT_ACCOUNT . $newId);
        $item->set($account);
        $this->setCurrentId($newId);

        return $this->cache->save($item);
    }

    /**
     * @param Account $account
     * @return bool
     */
    public function update(Account $account) : bool
    {
        $item = $this->cache->getItem(self::CACHE_KEY_PREFIX . self::CACHE_KEY_CURRENT_ACCOUNT . $account->getId());
        $item->set($account);
        return $this->cache->save($item);
    }
}