<?php

namespace App\Storage;

use App\Model\Account;

interface AccountStorageInterface
{
    /**
     * @return Account|null
     */
    public function getCurrent() : ?Account;

    /**
     * @param Account $account
     * @return bool
     */
    public function createNew(Account $account) : bool;

    /**
     * @param Account $account
     * @return bool
     */
    public function update(Account $account) : bool;

    /**
     * @return int|null
     */
    public function getCurrentId() : ?int;
}