<?php

namespace App\Model;

class Account
{
    const STATUS_OPEN = 1;
    const STATUS_CLOSED = 2;

    /** @var int */
    protected $id;

    /** @var int */
    protected $balance;

    /** @var int */
    protected $status;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Account
     */
    public function setId(int $id): Account
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getBalance(): int
    {
        return $this->balance;
    }

    /**
     * @param int $balance
     * @return Account
     */
    public function setBalance(int $balance): Account
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Account
     */
    public function setStatus(int $status): Account
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOpen() : bool
    {
        return $this->status == self::STATUS_OPEN;
    }
}
