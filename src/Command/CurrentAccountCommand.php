<?php

namespace App\Command;

use App\Exception\AccountException;
use App\Exception\InsufficientFundsAccountException;
use App\Exception\OverdraftMaxLimitException;
use App\Manager\AccountManager;
use App\Model\Account;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class CurrentAccountCommand extends ContainerAwareCommand
{
    /** @var QuestionHelper */
    protected $questionHelper;

    /** @var AccountManager */
    protected $accountManager;

    /** @var Account */
    protected $currentAccount;

    /** @var InputInterface */
    protected $input;

    /** @var OutputInterface */
    protected $output;

    protected function configure()
    {
        $this
            ->setName('app:current-account')
            ->setDescription("Current bank account CLI simulator")
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->accountManager = $this->getContainer()->get('account_manager');
        $this->questionHelper = $this->getHelper('question');
        $this->input = $input;
        $this->output = $output;

        $output->writeln('----[̲̅$̲̅(̲̅ ͡° ͜ʖ ͡°̲̅)̲̅$̲̅]----[̲̅$̲̅(̲̅ ͡° ͜ʖ ͡°̲̅)̲̅$̲̅]----[̲̅$̲̅(̲̅ ͡° ͜ʖ ͡°̲̅)̲̅$̲̅]----[̲̅$̲̅(̲̅ ͡° ͜ʖ ͡°̲̅)̲̅$̲̅]----');
        $output->writeln('Bank Account CLI Application. Made by Daumantas Urbanavičius.');

        $continue = true;
        while ($continue) {
            $continue = $this->chooseAction();
        }
    }

    /**
     * @return bool
     */
    private function chooseAction() : bool
    {
        $questionText = 'Please select account action:';
        if ($this->accountManager->isCurrentOpen()) {
            $this->currentAccount = $this->accountManager->getCurrent();
            $choices = [
                1 => 'View balance',
                2 => 'Withdraw funds',
                3 => 'Deposit funds',
                4 => 'Close account',
                5 => 'Exit'
            ];
        } else {
            $choices = [
                1 => 'Open account',
                2 => 'Exit'
            ];
        }

        $question = new ChoiceQuestion($questionText, $choices, 0);
        $question->setErrorMessage('Invalid choice: %s');
        $choice = $this->questionHelper->ask($this->input, $this->output, $question);
        $this->output->writeln('You have selected: '.$choice);

        return $this->executeAction($choice);
    }

    /**
     * @param string $choice
     * @return bool
     */
    private function executeAction(string $choice) : bool
    {
        try {
            switch ($choice) {
                case 'Open account':
                    $this->openAccount();
                    break;
                case 'View balance':
                    $this->viewBalance();
                    break;
                case 'Withdraw funds':
                    $this->withdrawFunds();
                    break;
                case 'Deposit funds':
                    $this->depositFunds();
                    break;
                case 'Close account':
                    $this->closeAccount();
                    break;
                case 'Exit':
                    $this->exit();

                    return false;
            }
        } catch (AccountException $exception) {
            $this->output->writeln('<error>Cannot complete account operation: ' . $exception->getMessage() . '</error>');
        } catch (\Exception $exception) {
            $this->output->writeln('<error>An error occurred.</error>');
        }

        return true;
    }

    /**
     * @throws \App\Exception\AccountStatusException
     */
    private function openAccount() : void
    {
        $this->accountManager->openCurrent();
        $this->output->writeln('Account opened successfully.');
    }

    private function viewBalance() : void
    {
        $balance = $this->currentAccount->getBalance();
        $this->output->writeln('Your balance is: '. $this->formatBalance($balance));
    }

    /**
     * @throws InsufficientFundsAccountException
     * @throws OverdraftMaxLimitException
     * @throws \App\Exception\AccountStatusException
     * @throws \App\Exception\BadAmountException
     */
    private function withdrawFunds() : void
    {
        $question = new Question('How much do you want to withdraw? ', '0');
        $question->setValidator([$this, 'isValidAmountInput']);
        $amount = $this->questionHelper->ask($this->input, $this->output, $question);
        $amountInCents = $this->parseAmountInput($amount);
        try {
            $this->accountManager->withdrawFunds($this->currentAccount, $amountInCents);
        } catch (InsufficientFundsAccountException $exception) {
            $this->output->writeln('<error>' . $exception->getMessage() . '</error>');
            $question = new ConfirmationQuestion('Would you like to apply overdraft? (y/n): ', true);
            $applyOverdraft = $this->questionHelper->ask($this->input, $this->output, $question);
            if (!$applyOverdraft) {

                return;
            }

            try {
                $this->accountManager->withdrawFunds($this->currentAccount, $amountInCents, true);
            } catch (OverdraftMaxLimitException $exception) {
                $this->output->writeln('<error>' . $exception->getMessage() . '</error>');

                return;
            }

            $this->output->writeln('Overdraft applied successfully.');
        }

        $this->output->writeln('Funds withdrawn successfully.');
    }

    /**
     * @throws \App\Exception\AccountDepositMaxLimitException
     * @throws \App\Exception\AccountStatusException
     * @throws \App\Exception\BadAmountException
     */
    private function depositFunds() : void
    {
        $question = new Question('How much do you want to deposit? ', '0');
        $question->setValidator([$this, 'isValidAmountInput']);
        $amount = $this->questionHelper->ask($this->input, $this->output, $question);
        $amountInCents = $this->parseAmountInput($amount);
        $this->accountManager->depositFunds($this->currentAccount, $amountInCents);
        $this->output->writeln('Funds deposited successfully.');
    }

    /**
     * @throws \App\Exception\AccountStatusException
     */
    private function closeAccount() : void
    {
        $this->accountManager->close($this->currentAccount);
        $this->output->writeln('Account closed successfully.');
    }

    /**
     * @param string $amount
     * @return string
     */
    public function isValidAmountInput(string $amount) : string
    {
        if (preg_match('/^[0-9]+((\.)[0-9]{1,2})?$/', $amount)) {
            return $amount;
        }

        throw new \RuntimeException(
            'Bad input format. Use number symbols and "." for delimiter, for example "100.05"'
        );
    }

    /**
     * @param int $balance
     * @return string
     */
    private function formatBalance(int $balance) : string
    {
        return  number_format($balance / 100.00, 2, '.', ',');
    }

    /**
     * @param string $amount
     * @return int
     */
    private function parseAmountInput(string $amount) : int
    {
        return (int) ((float) $amount * 100);
    }

    private function exit() : void
    {
        $this->output->writeln('Goodbye!');
    }
}